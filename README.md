# README #

### DB ###

* Файл **db/db.dat** - Конфигурация Oracle
* Файл **db/db.sql** - Скрипт для инициализации БД

### App ###

* **NET/frontend** - MVC
* **NET/backend** - Web API

### App configuration ###

##### Web API #####
* **Oracle__ConnectionString** - Настройка подключения к БД

##### Frontend #####
* **Backend__Scheme** - Схема подключения (http/https). http - по умолчанию
* **Backend__Host** - Хост для подключения к Web API
* **Backend__Port** - Порт для подключения к Web API

### Auth ###

* Логин:Пароль - admin:admin