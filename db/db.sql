CREATE TABLE users(
    reference char(36) NOT NULL,
    firstname varchar2(255) NOT NULL,
    lastname varchar2(255) NOT NULL,
    birthdate timestamp,
    phone number(11) NOT NULL,
    preferredcolors varchar2(255),
    preferreddrinks varchar2(255)
);

ALTER TABLE users
    ADD CONSTRAINT users_pkey PRIMARY KEY (reference);