﻿using Rusal.Frontend.Requests;

namespace Rusal.Frontend.MVC.Internal.Requests
{
    internal class GetUsersRequest : IGetUsersRequest
    {
        public string PreferredColor { get; set; }

        public string PreferredDrink { get; set; }
    }
}
