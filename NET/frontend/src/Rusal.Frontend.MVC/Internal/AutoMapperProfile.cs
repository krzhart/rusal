﻿using AutoMapper;
using Rusal.Frontend.MVC.Models;
using Rusal.Frontend.Users;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Rusal.Frontend.MVC.Internal
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserModel, User>()
                .ForMember(member => member.Phone, options => options.MapFrom(sMember => GetPhone(sMember.Phone)))
                .ForMember(member => member.Date, options => options.MapFrom(sMember => DateTime.ParseExact(sMember.Date, "dd.MM.yyyy", CultureInfo.InvariantCulture)));
        }

        private string GetPhone(string phone)
        {
            var regex = new Regex(@"[^\d]");

            return regex.Replace(phone, string.Empty);
        }
    }
}
