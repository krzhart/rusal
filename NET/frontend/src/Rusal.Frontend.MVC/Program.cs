﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Rusal.Frontend.MVC
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:3001")
                .UseStartup<Startup>();
    }
}
