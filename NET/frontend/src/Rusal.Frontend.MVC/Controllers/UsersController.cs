﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rusal.Frontend.MVC.Internal.Filters;
using Rusal.Frontend.MVC.Internal.Requests;
using Rusal.Frontend.MVC.Models;
using Rusal.Frontend.Store;
using Rusal.Frontend.Users;
using System;
using System.Threading.Tasks;

namespace Rusal.Frontend.MVC.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserStore _userStore;

        public UsersController(
            IUserStore userStore)
        {
            _userStore = userStore;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var users = await _userStore.GetList();

            var model = new UserListModel
            {
                Users = users
            };

            return View(model);
        }
        
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Index(UserListModel userListModel)
        {
            GetUsersRequest request = null;

            var filtersModel = userListModel.Filters;

            if (filtersModel != null)
            {
                request = new GetUsersRequest();

                if (!string.IsNullOrEmpty(filtersModel.PreferredColor))
                {
                    request.PreferredColor = filtersModel.PreferredColor;
                }

                if (!string.IsNullOrEmpty(filtersModel.PreferredDrink))
                {
                    request.PreferredDrink = filtersModel.PreferredDrink;
                }
            }

            var users = await _userStore.GetList(request);

            var model = new UserListModel
            {
                Users = users,
                Filters = filtersModel
            };

            return View(model);
        }
    }
}
