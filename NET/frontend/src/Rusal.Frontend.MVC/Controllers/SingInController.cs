﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Rusal.Frontend.MVC.Models;
using Rusal.Frontend.Proxy;
using Rusal.Frontend.Store;
using Rusal.Frontend.Users;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Rusal.Frontend.MVC.Controllers
{
    public class SingInController : Controller
    {
        private readonly IUserStore _userStore;
        private readonly IMapper _mapper;

        public SingInController(
            IUserStore userStore,
            IMapper mapper)
        {
            _userStore = userStore;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(SingInModel singInModel)
        {
            if (!(singInModel.Login.Equals("admin") ||
                singInModel.Password.Equals("admin")))
            {
                singInModel.IsSuccess = false;

                return View(singInModel);
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, singInModel.Login)
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddHours(1)
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);

            return RedirectToAction("Index", "Users");
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Index", "SingIn");
        }

        public IActionResult New()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> New(UserModel userModel)
        {
            if (!TryValidateModel(userModel, nameof(UserModel)))
            {
                return View();
            }

            var user = _mapper.Map<User>(userModel);

            try
            {
                await _userStore.Add(user);
            }
            catch (ProxyException ex)
            {
                return View("Error");
            }

            return View();
        }
    }
}
