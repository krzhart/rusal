﻿namespace Rusal.Frontend.MVC.Models
{
    public class SingInModel
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public bool IsSuccess { get; set; } = true;
    }
}
