﻿using Rusal.Frontend.Users;
using System.Collections.Generic;

namespace Rusal.Frontend.MVC.Models
{
    public class UserListModel
    {
        public IEnumerable<User> Users { get; set; }

        public UserListFiltersModel Filters { get; set; }
    }
}
