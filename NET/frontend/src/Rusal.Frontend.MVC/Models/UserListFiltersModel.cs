﻿namespace Rusal.Frontend.MVC.Models
{
    public class UserListFiltersModel
    {
        public string PreferredColor { get; set; }

        public string PreferredDrink { get; set; }
    }
}
