﻿using System.ComponentModel.DataAnnotations;

namespace Rusal.Frontend.MVC.Models
{
    public class UserModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [RegularExpression(@"(((0|1)[0-9]|2[0-9]|3[0-1]).(0[1-9]|1[0-2]).((19|20)\d\d))$")]
        public string Date { get; set; }

        [Phone]
        public string Phone { get; set; }

        public string[] PreferredColors { get; set; }

        public string[] PreferredDrinks { get; set; }
    }
}
