﻿namespace Rusal.Frontend.Proxy.Configuration
{
    public class BackendOptions
    {
        public string Scheme { get; set; } = "Http";

        public string Host { get; set; }

        public int Port { get; set; }
    }
}
