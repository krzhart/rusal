﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Rusal.Frontend.Proxy.Configuration;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Rusal.Frontend.Proxy.Internal
{
    internal class BackendProxy : IProxy
    {
        private readonly BackendOptions _backendOptions;

        public BackendProxy(IOptions<BackendOptions> options)
        {
            _backendOptions = options.Value;
        }

        public async Task<T> SendRequest<T>(HttpMethod method, string url, object payload = null)
        {
            var content = new StringContent(payload == null ? string.Empty : JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json");

            var requestMessage = new HttpRequestMessage()
            {
                Method = method,
                RequestUri = new Uri($"{_backendOptions.Scheme}://{_backendOptions.Host}:{_backendOptions.Port}{url}"),
                Content = content
            };

            var responseMessage = await Send(requestMessage);

            var result = await responseMessage.Content.ReadAsStringAsync();

            if (!responseMessage.IsSuccessStatusCode)
            {
                throw new ProxyException(responseMessage);
            }

            return JsonConvert.DeserializeObject<T>(result);

        }

        private async Task<HttpResponseMessage> Send(HttpRequestMessage requestMessage)
        {
            try
            {
                using (var httpClient = GetClient())
                {
                    return await httpClient.SendAsync(requestMessage);
                }

            }
            catch (Exception ex)
            {
                throw new ProxyException(ex);
            }
        }

        private HttpClient GetClient()
        {
            return new HttpClient();
        }
    }
}
