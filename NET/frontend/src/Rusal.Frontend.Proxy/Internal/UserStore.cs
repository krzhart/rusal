﻿using Rusal.Frontend.Requests;
using Rusal.Frontend.Store;
using Rusal.Frontend.Users;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Rusal.Frontend.Proxy.Internal
{
    internal class UserStore : IUserStore
    {
        private readonly IProxy _proxy;

        public UserStore(
            IProxy proxy)
        {
            _proxy = proxy;
        }

        public async Task Add(User user)
        {
            var result = await _proxy.SendRequest<User>(HttpMethod.Post, $"/api/users", user);
        }

        public async Task<User> Get(Guid reference)
        {
            return await _proxy.SendRequest<User>(HttpMethod.Get, $"/api/users/{reference}");
        }

        public async Task<IEnumerable<User>> GetList(IGetUsersRequest request = null)
        {
            var queryStringBuilder = new StringBuilder("/api/users?");

            if (request != null)
            {
                if (!string.IsNullOrEmpty(request.PreferredColor))
                {
                    queryStringBuilder.Append($"preferredColor={request.PreferredColor}&");
                }

                if (!string.IsNullOrEmpty(request.PreferredDrink))
                {
                    queryStringBuilder.Append($"preferredDrink={request.PreferredDrink}");
                }
            }

            var queryString = queryStringBuilder.ToString();

            return await _proxy.SendRequest<IEnumerable<User>>(HttpMethod.Get, queryString, request);
        }
    }
}
