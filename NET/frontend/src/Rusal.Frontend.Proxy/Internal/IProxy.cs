﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Rusal.Frontend.Proxy.Internal
{
    internal interface IProxy
    {
        Task<T> SendRequest<T>(HttpMethod method, string url, object payload = null);
    }
}
