﻿using Microsoft.Extensions.DependencyInjection;
using Rusal.Frontend.Proxy.Internal;
using Rusal.Frontend.Store;

namespace Rusal.Frontend.Proxy
{
    public static class ProxyExtensions
    {
        public static IServiceCollection AddBackendProxy(this IServiceCollection services)
        {
            services.AddScoped<IProxy, BackendProxy>();

            services.AddScoped<IUserStore, UserStore>();

            return services;
        }
    }
}
