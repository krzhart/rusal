﻿using System;
using System.Net.Http;

namespace Rusal.Frontend.Proxy
{
    public class ProxyException : Exception
    {
        public ProxyException(HttpResponseMessage responseFromProxy)
        {
            ProxyResponse = responseFromProxy;
        }

        public ProxyException(Exception exception)
        {
            var error = $"{exception.Message} {exception.InnerException}";

            Console.WriteLine(error);
        }

        public HttpResponseMessage ProxyResponse { get; private set; }
    }
}
