﻿using Rusal.Frontend.Requests;
using Rusal.Frontend.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rusal.Frontend.Store
{
    public interface IUserStore
    {
        Task Add(User user);

        Task<User> Get(Guid reference);

        Task<IEnumerable<User>> GetList(IGetUsersRequest request = null);
    }
}
