﻿using Rusal.Frontend.Users;

namespace Rusal.Frontend.Requests
{
    public interface IGetUsersRequest
    {
        string PreferredColor { get; }

        string PreferredDrink { get; }
    }
}
