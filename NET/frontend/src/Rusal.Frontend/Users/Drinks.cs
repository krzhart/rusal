﻿namespace Rusal.Frontend.Users
{
    public class Drinks
    {
        public const string Tea = "Tea";

        public const string Coffee = "Coffee";

        public const string Juice = "Juice";

        public const string Water = "Water";
    }
}
