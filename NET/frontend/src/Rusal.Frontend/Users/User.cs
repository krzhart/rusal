﻿using System;

namespace Rusal.Frontend.Users
{
    public class User
    {
        public Guid? Reference { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Date { get; set; }

        public string Phone { get; set; }

        public string[] PreferredColors { get; set; }

        public string[] PreferredDrinks { get; set; }
    }
}
