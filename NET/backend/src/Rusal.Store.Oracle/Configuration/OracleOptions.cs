﻿namespace Rusal.Store.Oracle.Configuration
{
    public class OracleOptions
    {
        public string ConnectionString { get; set; }
    }
}
