﻿using Microsoft.Extensions.DependencyInjection;
using Rusal.Backend.Store;
using Rusal.Store.Oracle.Internal;

namespace Rusal.Store.Oracle
{
    public static class OracleStoreExtensions
    {
        public static IServiceCollection AddOracleStore(this IServiceCollection services)
        {
            services.AddScoped<IUserStore, UserStore>();

            return services;
        }
    }
}
