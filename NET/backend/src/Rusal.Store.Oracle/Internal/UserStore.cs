﻿using Dapper;
using Microsoft.Extensions.Options;
using Rusal.Backend.Store;
using Rusal.Backend.Users;
using Rusal.Store.Oracle.Configuration;
using Rusal.Store.Oracle.Internal.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rusal.Store.Oracle.Internal
{
    internal class UserStore : OracleStore, IUserStore
    {
        private const string _select = @"SELECT u.reference as UserReference, u.firstname, u.lastname, u.birthdate, u.phone, u.preferredcolors as UserPreferredColors, u.preferreddrinks as UserPreferredDrinks 
                                         FROM users u ";
        private const char _separator = ',';
        public UserStore(IOptions<OracleOptions> options)
            : base(options)
        {
        }

        public async Task Add(User user)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync(
                    $@"INSERT INTO users(reference, firstname, lastname, birthdate, phone, preferredcolors, preferreddrinks)
	                   VALUES (:reference, :firstname, :lastname, :birthdate, :phone, :preferredcolors, :preferreddrinks)",
                    new
                    {
                        reference = user.Reference.ToString(),
                        firstname = user.FirstName,
                        lastname = user.LastName,
                        birthdate = user.Date,
                        phone = user.Phone,
                        preferredcolors = string.Join(_separator, user.PreferredColors),
                        preferreddrinks = string.Join(_separator, user.PreferredDrinks),
                    });
            }
        }

        public async Task<User> Get(Guid reference)
        {
            using (var connection = GetOpenConnection())
            {
                var result = await connection.QueryAsync<UserDb>(
                    $@"{_select} WHERE reference = :reference",
                    new
                    {
                        reference = reference.ToString(),
                    });

                return result.FirstOrDefault();
            }
        }

        public async Task<IEnumerable<User>> GetList(string preferredColor, string preferredDrink)
        {
            var queryBuilder = new StringBuilder($@"{_select}");

            if (!(string.IsNullOrEmpty(preferredColor) && string.IsNullOrEmpty(preferredDrink)))
            {
                queryBuilder.Append("WHERE ");

                if (!string.IsNullOrEmpty(preferredColor))
                {
                    queryBuilder.Append("u.preferredcolors LIKE '%' || :preferredcolor || '%'");
                    queryBuilder.Append(" AND ");
                }

                if (!string.IsNullOrEmpty(preferredDrink))
                {
                    queryBuilder.Append("u.preferreddrinks LIKE '%' || :preferreddrink || '%'");
                    queryBuilder.Append(" AND ");
                }

                queryBuilder = queryBuilder.Remove(queryBuilder.Length - 4, 4);
            }

            var query = queryBuilder.ToString();

            using (var connection = GetOpenConnection())
            {
                return await connection.QueryAsync<UserDb>(query,
                    new
                    {
                        preferredcolor = preferredColor,
                        preferreddrink = preferredDrink
                    });
            }
        }
    }
}
