﻿using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using Rusal.Store.Oracle.Configuration;
using System.Data;

namespace Rusal.Store.Oracle.Internal
{
    internal abstract class OracleStore
    {
        private readonly string _connectionString;

        protected OracleStore(IOptions<OracleOptions> options)
        {
            _connectionString = options.Value.ConnectionString;
        }

        protected IDbConnection GetOpenConnection()
        {
            var connection = new OracleConnection(_connectionString);
            connection.Open();

            return connection;
        }
    }
}
