﻿using Rusal.Backend.Users;
using System;

namespace Rusal.Store.Oracle.Internal.Dto
{
    internal class UserDb : User
    {
        private const char _separator = ',';

        internal string UserReference
        {
            get => Reference.ToString();
            set => Reference = new Guid(value);
        }

        internal string UserPreferredColors
        {
            get => string.Join(_separator, PreferredColors);
            set => PreferredColors = value.Split(_separator);
        }

        internal string UserPreferredDrinks
        {
            get => string.Join(_separator, PreferredDrinks);
            set => PreferredDrinks = value.Split(_separator);
        }
        internal DateTime BirthDate
        {
            get => Date;
            set => Date = value;
        }
    }
}
