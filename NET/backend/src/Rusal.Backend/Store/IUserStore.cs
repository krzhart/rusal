﻿using Rusal.Backend.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rusal.Backend.Store
{
    public interface IUserStore
    {
        Task Add(User user);

        Task<User> Get(Guid reference);

        Task<IEnumerable<User>> GetList(string preferredColor, string preferredDrink);
    }
}
