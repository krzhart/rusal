﻿using System;

namespace Rusal.Backend.Users
{
    public class User
    {
        public User()
        {
            Reference = Guid.NewGuid();
        }

        public Guid Reference { get; protected set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Date { get; set; }

        public string Phone { get; set; }

        public string[] PreferredColors { get; set; }

        public string[] PreferredDrinks { get; set; }
    }
}
