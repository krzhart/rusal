﻿using Rusal.Backend.Users;
using System;

namespace Rusal.Web.API.Internal.Users
{
    public class UserCreateRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Date { get; set; }

        public string Phone { get; set; }

        public string[] PreferredColors { get; set; }

        public string[] PreferredDrinks { get; set; }
    }
}
