﻿using Microsoft.AspNetCore.Mvc;
using Rusal.Backend.Store;
using Rusal.Backend.Users;
using Rusal.Web.API.Internal.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rusal.Web.API.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserStore _userStore;

        public UserController(
            IUserStore userStore)
        {
            _userStore = userStore;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetList(
            [FromQuery] string preferredColor,
            [FromQuery] string preferredDrink)
        {

            var users = await _userStore.GetList(preferredColor, preferredDrink);

            return Ok(users);
        }

        [HttpPost]
        public async Task<ActionResult<User>> Add(
            [FromBody] UserCreateRequest request)
        {
            var user = new User()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Date = request.Date,
                Phone = request.Phone,
                PreferredColors = request.PreferredColors,
                PreferredDrinks = request.PreferredDrinks,
            };


            await _userStore.Add(user);

            return Ok(user);
        }

        [HttpGet("{reference}")]
        public async Task<ActionResult<User>> Get(
            [FromQuery] Guid reference)
        {
            var user = await _userStore.Get(reference);

            return Ok(user);
        }
    }
}
